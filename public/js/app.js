/**
 * Angular main module
 *
 * @see Other stuff
 * @see Yet more stuff
 */

var app = angular.module('app', 
	[
		'ngRoute', 
		'controllers.home',
		'controllers.page'
	]
);

app.run(function($rootScope) {

	$rootScope.test = 1;

	$rootScope.increment = function() {
		$rootScope.test = $rootScope.test + 1;
	}

	$rootScope.showAlert = function(msg) {
		alert('Alert: ' + msg);
	};
});

app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider){

	$routeProvider

	.when('/',{
		controller: 'HomeController',
		templateUrl: 'views/home.html'
	})

	.otherwise({
		redirectTo: '/'
	});

	$locationProvider.html5Mode(true);
}]);
