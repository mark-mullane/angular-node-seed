var modulePage = angular.module('controllers.page', []);

// Route defined in the module...
modulePage.config(function($routeProvider) {	

	$routeProvider.when('/page',
		{ 
			controller: 'PageController', 
			templateUrl: 'views/page.html'
		});
});

modulePage.controller('PageController', function($scope, $rootScope){

	$scope.title = "A page of our app";	

	$scope.counter = function() {
		return $rootScope.test;
	};

	$scope.incrementTest = function() {
		$rootScope.increment();
	};


});

