angular.module('routes',[]).config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider){

	$routeProvider

	// Routes may be defined here, or in the module...
	.when('/',{
		controller: 'HomeController',
		templateUrl: 'views/home.html'
	})

	.otherwise({
		redirectTo: '/'
	});

	$locationProvider.html5Mode(true);
}]);