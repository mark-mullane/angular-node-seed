
// Jasmine Test Specification
describe('Sample Test Spec', function() {
	var foo;
	beforeEach(function() {
		foo = 0;
		foo += 1;
	});
	
	afterEach(function() {
		foo = 0;
	});
	
	it('should set foo to be 1', function() {
		expect(foo).toEqual(1);
	});
});
