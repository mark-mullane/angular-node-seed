﻿export interface IResponseHandler<T> {
    (response: T): void;
}
export interface IErrorHandler {
    (err: Error): void;
}
export declare class PBNBError implements Error {
    public message: string;
    public name: string;
    constructor(message: string);
}
