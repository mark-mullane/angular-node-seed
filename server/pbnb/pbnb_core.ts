﻿/// <reference path="../_all.d.ts" />

import $ = require("jquery");
import config = require("config");

//=====================================================================
// Internal Helpers
//=====================================================================

export interface IResponseHandler<T> { (response: T): void }
export interface IErrorHandler { (err: Error): void }

export class PBNBError implements Error {
    public name: string = "PBNB";
    constructor(public message: string) {
    }
}

export class PBNBResource<T> {

    private _uri: string;
    public Value: T;

    /**
    * Instantiate a new PassBridge NorthBound (PBNB) API Object wrapper class
    * This class exposes CRUD-like methods on the object
    */
    constructor(uri: string) {
        this._uri = config.API_ROOT + uri;
    }

    private authenticationRequired() {
        if ((this._uri.indexOf("/AuthToken") > 0) ||
            (this._uri.indexOf("/accountlogin") > 0) ||
            (this._uri.indexOf("/channellogin") > 0)) {
            return false;
        } else {
            if (PBNBAuthContext.authToken == null) {
                return true;
            } else {
                return false;
            }
        }
    }

    private beginRequest(uri: string, method: string, data: any, responseHandler: IResponseHandler<T>, errorHandler: IErrorHandler): JQueryPromise<T> {

        // Could return a Q.Promise<T> instead? 
        // JQuery promises do not fully adhere to the Promises/A spec, but are probably more familiar to 
        // potential consumers of the API.
        //var deferred = Q.defer<T>();
        //var promise = deferred.promise;
        var deferred = $.Deferred();
        var promise = deferred.promise();

        // Most requests require authentication. 
        // Make sure it is present before submitting the request
        if (this.authenticationRequired()) {
            var err = new PBNBError("Authentication required");
            if (errorHandler) errorHandler(err);
            deferred.reject(err);
            return promise;
        }

        // Configure the jQuery AJAX request...
        var ajax: JQueryAjaxSettings;
        ajax = {};
        ajax.url = uri;
        ajax.dataType = "json";
        ajax.timeout = config.HTTP_TIMEOUT;
        ajax.type = method.toUpperCase();
        ajax.data = data;

        if (PBNBAuthContext.authToken != null) {
            ajax.headers = { 'Authorization': 'Pbnb ' + PBNBAuthContext.authToken.Token };
        }

        ajax.success = (response: pbnb_models.PBNBReturn) => {

            if (response.Success) {
                this.Value = <T>response.Data;
                if (responseHandler) {
                   responseHandler(this.Value);
                }
                deferred.resolve(this.Value);
            } else {
                this.Value = null;
                var err = new PBNBError(response.ErrorDescription + " [" + response.ErrorCode + "]")
                if (errorHandler) {
                    errorHandler(err);
                }
                deferred.reject(err);
            }
        };

        ajax.error = (xhr, msg, e) => {
            this.Value = null;
            var err = new Error();
            err.name = "HTTP";
            if (e) {
                err.message = e + " [" + xhr.status + "]";
            } else if (msg) {
                err.message = msg;
            } else {
                err.message = xhr.statusText;
            }
            if (errorHandler) {
               errorHandler(err);
            }
            deferred.reject(err);
        };

        $.ajax(ajax);
        return promise;
    }

    /**
     * GET (Read) resource data, and invoke callbacks if defined.
     * Returns a jQuery promise.
     * 
     * @param responseHandler Optional success callback.
     * @param errorHandler Optional error callback.
     */
    public Get(responseHandler: IResponseHandler<T> = null, errorHandler: IErrorHandler = null): JQueryPromise<T> {
        return this.beginRequest(this._uri, "GET", null, responseHandler, errorHandler);
    }

    /**
     * POST (Update) resource data, and invoke callbacks if defined.
     * Returns a jQuery promise.
     * 
     * @param data Data to post.
     * @param responseHandler Optional success callback.
     * @param errorHandler Optional error callback.
     */
    public Post(data: any, responseHandler: IResponseHandler<T> = null, errorHandler: IErrorHandler = null): JQueryPromise<T> {
        return this.beginRequest(this._uri, "POST", data, responseHandler, errorHandler);
    }
}

//=====================================================================
// Authentication Service
//=====================================================================

export class PBNBAuthContext {
    static authToken: pbnb_models.AuthToken = null;
    static accountID: number = -1;
}

export class PBNBCredentials {
    Username: string;
    Password: string;
}

export interface IAuthTokenCallback { (authToken: pbnb_models.AuthToken): void }
export class PBNBAuthenticationService {

    private _uri: string;
    private _creds: PBNBCredentials;
    private _callback: IAuthTokenCallback;
    private _renewTimer: number;
    private _autoRenew: boolean;
    private _error: Error;

    constructor() {
        this._uri = null;
        this._creds = null;
        this._autoRenew = false;
        this._callback = null;
        this._renewTimer = 0;
        this._error = null;
    }

    private errorHandler(err: Error) {
        this._error = err;
        PBNBAuthContext.authToken = null;
        PBNBAuthContext.accountID = -1;
        if (this._callback) this._callback(null);
    }

    private tokensHandler(tokens: pbnb_models.AuthToken[]) {
        var token = tokens[0];
        (new PBNBResource<pbnb_models.AuthToken>(this._uri)).Post(token)
            .done((data) => this.tokenHandler(data))
            .fail((err) => this.errorHandler(err));
    }

    private tokenHandler(token: pbnb_models.AuthToken) {
        if (token == null || token.Token.length == 0) {
            throw new PBNBError("No token available?");
        } else {
            if (this._autoRenew) {
                // Got a new token...parse it to get the expiry time.
                var now = Date.parse(token.Now);
                var expires = Date.parse(token.Expires);
                var interval = (expires - now) * 0.8;
                this._renewTimer = setTimeout(() => { this.beginTokenRequest() }, interval);
            }
            this.onTokenUpdate(token);
        }
    }

    private loginHandler(token: pbnb_models.AuthToken) {
        if (token == null || token.Token.length == 0) {
            throw new PBNBError("No token available?");
        } else {
            // Got a new token...parse it to get the expiry time.
            if (this._autoRenew) {
                var now = Date.parse(token.Now);
                var expires = Date.parse(token.Expires);
                var interval = (expires - now) * 0.8;
                this._renewTimer = setTimeout(() => { this.beginLoginRequest() }, interval);
            }
            this.onTokenUpdate(token);
        }
    }

    private beginTokenRequest() {
        (new PBNBResource<pbnb_models.AuthToken[]>(this._uri)).Get()
            .done((data) => this.tokensHandler(data))
            .fail((err) => this.errorHandler(err));
    }

    private beginLoginRequest() {
        (new PBNBResource<pbnb_models.AuthToken>(this._uri)).Post(this._creds)
            .done((data) => this.loginHandler(data))
            .fail((err) => this.errorHandler(err));
    }

    private onTokenUpdate(token: pbnb_models.AuthToken) {
        // Update globals...
        PBNBAuthContext.authToken = token;
        if (token.Role == "account") {
            PBNBAuthContext.accountID = parseInt(token.Scope);
        } else {
            PBNBAuthContext.accountID = -1;
        }

        // Callback if set...
        if (this._callback) this._callback(token);
    }

    public GetAppToken(appID: string, appSecret: string, onTokenAvailable: IAuthTokenCallback, autoRenew: boolean = true): void {
        this._uri = "v1/core/authtoken?appID=" + appID + "&secret=" + appSecret;
        this._callback = onTokenAvailable;
        this._autoRenew = autoRenew;
        this._error = null;
        if (this._renewTimer) {
            clearTimeout(this._renewTimer);
            this._renewTimer = 0;
        }
        this.beginTokenRequest();
    }

    public GetAdminToken(username: string, password: string, onTokenAvailable: IAuthTokenCallback, autoRenew: boolean = true): void {
        this._uri = "v1/core/accountlogin";

        this._creds = new PBNBCredentials();
        this._creds.Username = username;
        this._creds.Password = password;

        this._callback = onTokenAvailable;
        this._autoRenew = autoRenew;
        this._error = null;
        if (this._renewTimer) {
            clearTimeout(this._renewTimer);
            this._renewTimer = 0;
        }
        this.beginLoginRequest();
    }

    public Logout() {
        PBNBAuthContext.authToken = null;
        PBNBAuthContext.accountID = -1;
        if (this._callback) {
            this._callback(null);
        }
        if (this._renewTimer) {
            clearTimeout(this._renewTimer);
            this._renewTimer = 0;
        }
        this._creds = null;
        this._callback = null;
        this._autoRenew = false;
        this._error = null;
    }

    public GetLastError(): Error {
        return this._error;
    }
}
