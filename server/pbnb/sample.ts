﻿
//=====================================================================
// Internal Helpers
//=====================================================================

export interface IResponseHandler<T> { (response: T): void }
export interface IErrorHandler { (err: Error): void }

export class PBNBError implements Error {
    public name: string = "PBNB";
    constructor(public message: string) {
    }
}