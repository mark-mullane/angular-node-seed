pbnb = require('../pbnb/sample');

exports.redirect = function(req, res) {
	res.redirect('/');
}

exports.notfound = function(req, res) {
	var p = new pbnb.PBNBError("xxxx?");
	res.send(404, 'Not found? ' + p.message);
}

exports.api = function(req, res, next) {
	var context = req.params.context;
	res.send('API Handler! (' + context + ')');
}

