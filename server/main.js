var express = require('express'),
	bodyParser = require('body-parser'),
	routes = require('./routes'),
	logger = require('morgan'),
	path = require('path');
	
var app = module.exports = express();


// Config

var port = process.argv[2] || 9000;

var env = process.env.NODE_ENV || 'development';

if('development' == env){
	app.use(logger('dev'));
	app.use('/', express.static(path.join(__dirname, '../public')));
}

// Routes

// index 
app.use('/api/:context', routes.api);

//default
//app.use('*', routes.notfound);
app.all('/*', function(req, res, next) {
    // Just send the app page for other files to support HTML5Mode
    res.sendfile('index.html', { root: path.join(__dirname, '../public')});
});

// Start!
app.listen(port);
console.log('Server foo started on port '+ port);
