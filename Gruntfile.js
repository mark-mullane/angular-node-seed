module.exports = function (grunt) {

  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

  grunt.initConfig({
 
    'pkg': grunt.file.readJSON('package.json'),
	   
    'bower': grunt.file.readJSON('.bowerrc'),
	
    'meta': {
  	  'jsFilesForTesting': [
          '<%= bower.directory %>/jquery/jquery.js',
          '<%= bower.directory %>/angular/angular.js',
          '<%= bower.directory %>/angular-route/angular-route.js',
          '<%= bower.directory %>/angular-sanitize/angular-sanitize.js',
          '<%= bower.directory %>/angular-mocks/angular-mocks.js',
          '<%= bower.directory %>/restangular/dist/restangular.js',
          '<%= bower.directory %>/underscore/underscore.js',
          'test/specs/**/*.js'
        ]
    },

  	'clean': {
        all: {
          files: [{
            dot: true,
            src: [
              '.tmp',
              'dist',
              'doc/{,*/}*',
              '!doc/.git*'
            ]
          }]
        },
        server: '.tmp'
      },	
	
  	'typescript': {
  		base: {
  			src: ['server/pbnb/sample.ts'],
  			options: {
  				module: 'commonjs',
  				target: 'es5',
  				sourceMap: true,
  				declaration: true
  			}
  		}
  	},
  	
  	'jshint': {
    	'options': {
  			'reporter': require('jshint-stylish')
   		},  
     	'beforeconcat': ['public/js/*.js', 'test/**/*.js']
    },
  	
   
    'karma': {
        
      'development': {
          'configFile': 'test/karma.conf.js',
          'options': {
            'files': [
              '<%= meta.jsFilesForTesting %>',
              'public/js/*.js'
            ],
          }
      },
  	  
      'dist': {
          'options': {
            'configFile': 'test/karma.conf.js',
            'files': [
              '<%= meta.jsFilesForTesting %>',
              'dist/<%= pkg.namelower %>-<%= pkg.version %>.js'
            ],
          }
        },
  	  
      'minified': {
         'options': {
            'configFile': 'test/karma.conf.js',
            'files': [
              '<%= meta.jsFilesForTesting %>',
              'dist/<%= pkg.namelower %>-<%= pkg.version %>.min.js'
            ],
          }
       }
    },

    'concat': {
      'dist': {
        'src': ['public/*.js'],
        'dest': 'dist/<%= pkg.namelower %>-<%= pkg.version %>.js'
      }
    },

    'uglify': {
      'options': {
        'mangle': false
      },  
      'dist': {
        'files': {
          'dist/<%= pkg.namelower %>-<%= pkg.version %>.min.js': ['dist/<%= pkg.namelower %>-<%= pkg.version %>.js']
        }
      }
    },

    'jsdoc': {
      'src': ['public/*.js'],
      'options': {
        'destination': 'doc'
      }
    },

    'express': {
      'options' :{
      },
      'dev': {
         'options' :{    
          'script': 'server/main.js'
        }        
      }
    },

    'open' : {
      'dev': {
        'path': 'http://localhost:9000/',
        'app': 'Chrome'
      }
    },


    'watch': {

      'server': {
        'files': ['server/**/*.*'],
        'tasks': ['express:dev'],
        'options': {
           spawn: false,
        }
      },
      'public' : {
        'files': ['public/**/*.*'],
        'options' : { livereload: true },
      }
    }    

  });

  grunt.registerTask('test', ['jshint', 'karma:development']);
  grunt.registerTask('build',
    [
      'jshint',
      'karma:development',
      'concat',
      'karma:dist',
      'uglify',
      'karma:minified',
      'jsdoc'
    ]);

  grunt.registerTask('dev', ['express:dev', 'open', 'watch']);

};
