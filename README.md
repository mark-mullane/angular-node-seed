# README #

Seed for an Angular/Node JS project, using Grunt as a task-runner, Karma for testing, and Bower for for client-side package management. Note that this is more concerned with scaffolding than the actual Angular (client-side) or NodeJS (server-side) applications themselves, hence the trivial app. 

## Features: ##
Grunt support for common workflows, including auto-restart of server of server-side code changes, and auto-refresh of browser on client-side code changes.

## Requirements: ##
Node.js, with Grunt Cli and Bower modules globally installed. 

Also, the [LiveReload browser extension for Chrome](https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei?hl=en) is required for automatic reloading on code changes.

### Installing Grunt Cli ###
The Grunt Command Line Interface is a Node.js module which allows us to execute the Grunt tasks of our project on the command line. To install (only needs to be done once per dev environment):

```
#!javascript
npm install -g grunt-cli
```

### Installing Bower: ###
Bower is a package and dependency manager for frontend JavaScript libraries (like NPM for the client-side). To install (only needs to be done once per dev environment):

```
#!javascript
npm install -g bower
```

## Prepare the seed: ##

```
#!javascript
npm install
```

## Develop: ##
Run the grunt task below to open a Chrome browser with the seed Angular app. You will need to manually enable the LiveReload extension on the opened page. Try editing a client-side file and the page will reload. Editing a server-side file will cause the server to automatically restart. Enjoy!

```
#!javascript
grunt dev
```
